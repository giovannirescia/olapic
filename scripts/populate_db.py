# -*- coding: utf-8 -*-

"""Script for populate the database."""
from os.path import join

from dateutil.parser import parse
import pandas as pd
from sqlalchemy import create_engine

from conversion_funnel.models import (Customer, Event, User, EventType,
                                      Device)
from conversion_funnel.database import db_session, init_db
from settings import CSV_DATA_PATH, APP_DATA

SQLITEDB_PATH = join(APP_DATA, "olapic.db")

ENGINE = create_engine("sqlite:///{}".format(SQLITEDB_PATH), convert_unicode=True)


def read_from_csv(fpath):
    """Reads a csv file -dataset- and loads the data into the database."""
    df = pd.read_csv(fpath, header=None, sep="|")
    customers = list(df[0].unique())
    users = list(df[2].unique())
    event_types = list(df[3].unique())
    devices = list(df[4].unique())

    print("\nAdding customers...\n")
    for customer in customers:
        obj = Customer(id=int(customer))
        db_session.add(obj)
    print("\t{0} / {0}".format(len(customers)))
    db_session.commit()

    print("\nAdding event types...\n")
    for event_type in event_types:
        obj = EventType(id=int(event_type))
        db_session.add(obj)
    print("\t{0} / {0}".format(len(event_types)))
    db_session.commit()

    print("\nAdding users...\n")
    total_users = len(users)
    for i, user in enumerate(users, start=1):
        obj = User(id=str(user))
        db_session.add(obj)
        if i % 50000 == 0 or i == total_users:
            print("\t{} / {}".format(i, total_users))
            db_session.commit()

    print("\nAdding devices...\n")
    for device in devices:
        obj = Device(id=int(device))
        db_session.add(obj)
    print("\t{0} / {0}".format(len(devices)))
    db_session.commit()

    events = []
    print("\nAdding events...\n")
    total_rows = df.shape[0]

    for i, row in df.iterrows():
        customer_id, raw_timestamp, user_id, event_type_id, device_id = row
        timestamp = parse(raw_timestamp)
        events.append({"customer_id": customer_id,
                       "timestamp": timestamp,
                       "user_id": user_id,
                       "event_type_id": event_type_id,
                       "device_id": device_id,
                       })

        if (i + 1) % 50000 == 0 or i + 1 == total_rows:
            ENGINE.execute(
                Event.__table__.insert(),
                events
            )
            events = []
            print("\t{} / {}".format(i + 1, total_rows))

    db_session.commit()
    db_session.close()


if __name__ == "__main__":
    init_db()
    read_from_csv(CSV_DATA_PATH)
