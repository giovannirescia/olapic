#!/bin/bash

dir=data
file=test_hits.txt

if [ ! -d "$dir" ]; then
    mkdir data
fi
cd data
if [ ! -f "$file" ]; then
    wget https://s3.amazonaws.com/olapic-jobs-tests/test_hits.txt
fi
filesize=$(wc -c < "$file")
if [ $filesize -eq 69297910 ]; then
    echo "File found and verified, ready to go!"
else
    wget https://s3.amazonaws.com/olapic-jobs-tests/test_hits.txt
    filesize=$(wc -c < "$file")
    if [ $filesize -eq 69297910 ]; then
	echo "File found and verified, ready to go!"
    else
	echo "WARNING! File size doesn't match, run this script again!"
    fi
fi
