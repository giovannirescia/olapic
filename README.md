# Olapic Data Engineering Project #

#### Conversion Funnel Metrics
Build a system that computes and makes it available the results of a conversion funnel.

#### What's a conversion funnel?

A conversion funnel describes the sequence of steps in a user's journey to reach the checkout page. The shape of a funnel represents the gradual decline in the number of users who reach each step. Not all site visitors continue through to making a purchase.

### Design

Given the nature of the problem, SQLAlchemy and Flask were the chosen tools for providing a solution. [Flask counts with several design patterns for use with SQLAlchemy](http://flask.pocoo.org/docs/0.12/patterns/sqlalchemy), the [declarative pattern](http://flask.pocoo.org/docs/0.12/patterns/sqlalchemy/#declarative) is the one used in this implementation with a mid-layer between the database and the API that handles the logic, as can be seen in the diagram. Also, the API Exceptions were implemented following the [patterns that Flask provides](http://flask.pocoo.org/docs/0.12/patterns/apierrors/).

![system_diag.png](https://bitbucket.org/repo/BLAMjj/images/1826119567-system_diag.png)

## Usage

### Requirements ###
* Python 3.5
* SQLite
* conda
* git
 
### Installation instructions ###
`git clone git@bitbucket.org:giovannirescia/olapic.git`

`conda create --name olapic python=3.5 ipykernel`

`pip install -r requirements.txt`

#### Set up

1. Instantiate `settings.py.template` if you want to customize some paths, if not, just run `cp settings.py.template settings.py`.
2. Run `sh scripts/init.sh`, to download the dataset `test_hits.txt` in the directory `data` which will be also created.
3. Run `python scripts/populate_db.py` to create the database and populate it with the data in `data/test_hits.txt`, the database will be located in `data/olapic.db`.

#### Web Service

Run `python conversion_funnel/api_funnel.py [-h] [-H HOST] [-P PORT] [-D DEBUG]`, where:

   *  `-h, --help`
   * `-H HOST, --host HOST`  The host to run this server on (default: 0.0.0.0)
   * `-P PORT, --port PORT`  The port to run this server on (default: 5000)
   * `-D DEBUG, --debug DEBUG` Debug mode (default: 0)

Once the web service is up and running, you should be able to run the next REST command:
   - `GET olapic/api/v1.0/customers/{customer_id}/conversion_funnel{?date_from,date_to}`

For example, you could use `curl` to run it:

 - `curl -XGET http://0.0.0.0:5000/olapic/api/v1.0/customers/216182/conversion_funnel`
 - Output:
```
{
  "data": {
    "customer": [
      "216182"
    ], 
    "conversion_funnel": {
      "step_1": 674092, 
      "step_2": 24082,
      "step_3": 678
    }
  }
}
```
 - `curl -XGET http://0.0.0.0:5000/olapic/api/v1.0/customers/216009/conversion_funnel?date_from\=2015-10-03-22:35:12.887`
 - Output:
```
 {
  "data": {
    "customer": [
      "216009"
    ], 
    "conversion_funnel": {
      "step_1": 265012, 
      "step_2": 20629,
      "step_3": 299
    }
  }
}
```
 - `curl -XGET http://0.0.0.0:5000/olapic/api/v1.0/customers/216182/conversion_funnel\?date_to\=2015-10-04-07:21:55.01`
 - Output:
```
{
  "data": {
    "customer": [
      "216182"
    ], 
    "conversion_funnel": {
      "step_1": 215138, 
      "step_2": 8105,
      "step_3": 176
    }
  }
}
```
 - `curl -XGET http://0.0.0.0:5000/olapic/api/v1.0/customers/216009/conversion_funnel\?date_from\=2015-10-03-22:35:12.887\&date_to\=2015-10-4-06`
 - Output:
```
{
  "data": {
    "customer": [
      "216009"
    ], 
    "conversion_funnel": {
      "step_1": 14993, 
      "step_2": 1411,
      "step_3": 19
    }
  }
}
```


### Tests

There are unit tests for the data access (the web service tests were removed since they were integration tests rather than unit tests). To run the tests, just run `python nosetests`.