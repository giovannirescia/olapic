# -*- coding: utf-8 -*-

"""Unit tests for the data access layer."""
import unittest
from datetime import datetime
from dateutil.parser import parse

from sqlalchemy import create_engine

from conversion_funnel.data_access import DataAccess
from conversion_funnel.database import init_db
from conversion_funnel.models import Event, EventType, Customer


class BaseTestCase(unittest.TestCase):
    """This is a base class for the test cases.

    All other classes inherit from this class
    """
    # Stolen from: https://gist.github.com/twolfson/13f5f5784f67fd49b245
    @classmethod
    def setUpClass(cls):
        """On inherited classes, run our `setUp` method."""
        if cls is not BaseTestCase and cls.setUp is not BaseTestCase.setUp:
            orig_setUp = cls.setUp

            def setUpOverride(self, *args, **kargs):
                BaseTestCase.setUp(self)
                return orig_setUp(self, *args, **kargs)
            cls.setUp = setUpOverride

    def setUp(self):
        engine = create_engine("sqlite:///", convert_unicode=True)
        init_db(engine)
        self.data_access = DataAccess(engine)

        EVENT_TYPES = list(map(lambda x: EventType(id=x), [11, 13, 14, 15, 1]))

        CUSTOMERS = list(map(lambda x: Customer(id=x), ["adibas", "nkie"]))

        EVENTS = [
            # adibas
            Event(customer_id="adibas", timestamp=parse("2005-05-13 00:00:00"), user_id="messi", event_type_id=11),
            Event(customer_id="adibas", timestamp=parse("2005-05-13 00:10:00"), user_id="messi", event_type_id=15),
            Event(customer_id="adibas", timestamp=parse("2005-05-13 00:20:00"), user_id="messi", event_type_id=1),
            Event(customer_id="adibas", timestamp=parse("2005-05-14 00:00:00"), user_id="randUser_1", event_type_id=11),
            Event(customer_id="adibas", timestamp=parse("2005-05-14 00:10:00"), user_id="randUser_1", event_type_id=13),
            Event(customer_id="adibas", timestamp=parse("2005-05-14 00:20:00"), user_id="randUser_1", event_type_id=1),
            Event(customer_id="adibas", timestamp=parse("2005-05-15 00:00:00"), user_id="randUser_2", event_type_id=11),
            Event(customer_id="adibas", timestamp=parse("2005-05-15 00:10:00"), user_id="randUser_2", event_type_id=14),
            Event(customer_id="adibas", timestamp=parse("2005-05-16 00:00:00"), user_id="randUser_3", event_type_id=11),
            Event(customer_id="adibas", timestamp=parse("2005-05-17 00:10:00"), user_id="randUser_4", event_type_id=11),
            Event(customer_id="adibas", timestamp=parse("2005-05-17 00:20:00"), user_id="randUser_4", event_type_id=14),
            Event(customer_id="adibas", timestamp=parse("2005-05-17 00:30:00"), user_id="randUser_4", event_type_id=1),
            Event(customer_id="adibas", timestamp=parse("2005-05-18 00:00:00"), user_id="randUser_5", event_type_id=11),
            Event(customer_id="adibas", timestamp=parse("2005-05-18 00:10:00"), user_id="randUser_5", event_type_id=13),
            Event(customer_id="adibas", timestamp=parse("2005-05-19 00:00:00"), user_id="randUser_6", event_type_id=11),
            Event(customer_id="adibas", timestamp=parse("2005-05-19 00:10:00"), user_id="randUser_6", event_type_id=13),
            Event(customer_id="adibas", timestamp=parse("2005-05-19 00:20:00"), user_id="randUser_6", event_type_id=1),
            Event(customer_id="adibas", timestamp=parse("2005-05-20 00:00:00"), user_id="randUser_7", event_type_id=11),
            Event(customer_id="adibas", timestamp=parse("2005-05-20 00:10:00"), user_id="randUser_7", event_type_id=15),
            Event(customer_id="adibas", timestamp=parse("2005-05-21 00:00:00"), user_id="randUser_8", event_type_id=11),
            Event(customer_id="adibas", timestamp=parse("2005-05-21 00:10:00"), user_id="randUser_8", event_type_id=15),
            Event(customer_id="adibas", timestamp=parse("2005-05-22 00:00:00"), user_id="randUser_9", event_type_id=11),
            Event(customer_id="adibas", timestamp=parse("2005-05-23 00:00:00"), user_id="randUser_10", event_type_id=11),
            # nkie
            Event(customer_id="nkie", timestamp=parse("2005-06-13 00:00:00"), user_id="jordan", event_type_id=11),
            Event(customer_id="nkie", timestamp=parse("2005-06-13 00:10:00"), user_id="jordan", event_type_id=13),
            Event(customer_id="nkie", timestamp=parse("2005-06-13 00:20:00"), user_id="jordan", event_type_id=1),
            Event(customer_id="nkie", timestamp=parse("2005-06-14 00:00:00"), user_id="randUser_1", event_type_id=11),
            Event(customer_id="nkie", timestamp=parse("2005-06-15 00:00:00"), user_id="randUser_2", event_type_id=11),
            Event(customer_id="nkie", timestamp=parse("2005-06-16 00:00:00"), user_id="randUser_3", event_type_id=11),
            Event(customer_id="nkie", timestamp=parse("2005-06-16 00:10:00"), user_id="randUser_3", event_type_id=13),
            Event(customer_id="nkie", timestamp=parse("2005-06-17 00:10:00"), user_id="randUser_4", event_type_id=11),
            Event(customer_id="nkie", timestamp=parse("2005-06-17 00:20:00"), user_id="randUser_4", event_type_id=14),
            Event(customer_id="nkie", timestamp=parse("2005-06-18 00:00:00"), user_id="randUser_5", event_type_id=11),
            Event(customer_id="nkie", timestamp=parse("2005-06-18 00:10:00"), user_id="randUser_5", event_type_id=15),
            Event(customer_id="nkie", timestamp=parse("2005-06-18 00:20:00"), user_id="randUser_5", event_type_id=1),
            Event(customer_id="nkie", timestamp=parse("2005-06-19 00:00:00"), user_id="randUser_6", event_type_id=11),
            Event(customer_id="nkie", timestamp=parse("2005-06-19 00:10:00"), user_id="randUser_6", event_type_id=13),
            Event(customer_id="nkie", timestamp=parse("2005-06-19 00:20:00"), user_id="randUser_6", event_type_id=1),
            Event(customer_id="nkie", timestamp=parse("2005-06-20 00:00:00"), user_id="randUser_7", event_type_id=11),
            Event(customer_id="nkie", timestamp=parse("2005-06-20 00:10:00"), user_id="randUser_7", event_type_id=15),
            Event(customer_id="nkie", timestamp=parse("2005-06-20 00:20:00"), user_id="randUser_7", event_type_id=1),
            Event(customer_id="nkie", timestamp=parse("2005-06-21 00:00:00"), user_id="randUser_8", event_type_id=11),
            Event(customer_id="nkie", timestamp=parse("2005-06-22 00:00:00"), user_id="randUser_9", event_type_id=11),
            Event(customer_id="nkie", timestamp=parse("2005-06-22 00:10:00"), user_id="randUser_9", event_type_id=14),
            Event(customer_id="nkie", timestamp=parse("2005-06-23 00:00:00"), user_id="randUser_10", event_type_id=11),
            Event(customer_id="nkie", timestamp=parse("2005-06-23 00:10:00"), user_id="randUser_10", event_type_id=15),
            Event(customer_id="nkie", timestamp=parse("2005-06-24 00:00:00"), user_id="randUser_11", event_type_id=11),
            Event(customer_id="nkie", timestamp=parse("2005-06-24 00:10:00"), user_id="randUser_11", event_type_id=14),
            Event(customer_id="nkie", timestamp=parse("2005-06-24 00:20:00"), user_id="randUser_11", event_type_id=1),
        ]

        for obj in EVENT_TYPES + CUSTOMERS + EVENTS:
            self.data_access.add_object(obj)


class DataAccessGetUsers(BaseTestCase):
    """Test the total users."""
    def test_get_total_users_for_each_step(self):
        steps_dict = {"step_1": [11], "step_2": [13, 14, 15], "step_3": [1]}

        # customer adibas
        model_total_users_step_1 = self.data_access.get_users_report(customer_id="adibas", event_types=steps_dict)
        gold_total_users_step_1 = 11
        self.assertEqual(gold_total_users_step_1, model_total_users_step_1["step_1"])

        model_total_users_step_2 = self.data_access.get_users_report(customer_id="adibas", event_types=steps_dict)
        gold_total_users_step_2 = 8
        self.assertEqual(gold_total_users_step_2, model_total_users_step_2["step_2"])

        model_total_users_step_3 = self.data_access.get_users_report(customer_id="adibas", event_types=steps_dict)
        gold_total_users_step_3 = 4
        self.assertEqual(gold_total_users_step_3, model_total_users_step_3["step_3"])

        # customer nkie
        model_total_users_step_1 = self.data_access.get_users_report(customer_id="nkie", event_types=steps_dict)
        gold_total_users_step_1 = 12
        self.assertEqual(gold_total_users_step_1, model_total_users_step_1["step_1"])

        model_total_users_step_2 = self.data_access.get_users_report(customer_id="nkie", event_types=steps_dict)
        gold_total_users_step_2 = 9
        self.assertEqual(gold_total_users_step_2, model_total_users_step_2["step_2"])

        model_total_users_step_3 = self.data_access.get_users_report(customer_id="nkie", event_types=steps_dict)
        gold_total_users_step_3 = 5
        self.assertEqual(gold_total_users_step_3, model_total_users_step_3["step_3"])

    def test_get_total_users_for_each_step_with_filtering_by_date(self):
        steps_dict = {"step_1": [11], "step_2": [13, 14, 15], "step_3": [1]}

        # customer adibas
        model_total_users_step_1 = self.data_access.get_users_report(customer_id="adibas", event_types=steps_dict, date_from="2005-05-19 00:05:3")
        gold_total_users_step_1 = 4
        self.assertEqual(gold_total_users_step_1, model_total_users_step_1["step_1"])

        model_total_users_step_2 = self.data_access.get_users_report(customer_id="adibas", event_types=steps_dict, date_from="2005-05-17 17:44:08.992")
        gold_total_users_step_2 = 4
        self.assertEqual(gold_total_users_step_2, model_total_users_step_2["step_2"])

        model_total_users_step_3 = self.data_access.get_users_report(customer_id="adibas", event_types=steps_dict, date_from="2005-05-14 12:33", date_to="2005-05-15 00:9")
        gold_total_users_step_3 = 0
        self.assertEqual(gold_total_users_step_3, model_total_users_step_3["step_3"])

        # customer nkie
        model_total_users_step_1 = self.data_access.get_users_report(customer_id="nkie", event_types=steps_dict, date_to="2005-06-14")
        gold_total_users_step_1 = 2
        self.assertEqual(gold_total_users_step_1, model_total_users_step_1["step_1"])

        model_total_users_step_2 = self.data_access.get_users_report(customer_id="nkie", event_types=steps_dict, date_from="2005-06-15 00:15", date_to="2005-06-21 12:09")
        gold_total_users_step_2 = 5
        self.assertEqual(gold_total_users_step_2, model_total_users_step_2["step_2"])

        model_total_users_step_3 = self.data_access.get_users_report(customer_id="nkie", event_types=steps_dict, date_to="2005-06-16 00:43:55.223")
        gold_total_users_step_3 = 1
        self.assertEqual(gold_total_users_step_3, model_total_users_step_3["step_3"])

    def test_when_a_bad_id_type_is_given_then_a_type_error_is_raise(self):
        # customers
        bad_customers_ids = [parse, 1, dict(), ]
        for bad_id in bad_customers_ids:
            self.assertRaises(TypeError, self.data_access.get_users_report, bad_id, [])
        # events
        bad_events_ids = {"step_1": ["1", unittest, Exception, ()]}
        self.assertRaises(TypeError, self.data_access.get_users_report, "nkie", bad_events_ids)

    def test_when_a_bad_date_format_or_range_is_given_then_a_value_error_is_raised(self):
        bad_format_date = "11|03|2015"
        self.assertRaises(ValueError, self.data_access.get_users_report, "nkie", {}, bad_format_date)

        bad_format_date = "2022 3 12 15 45 22"
        self.assertRaises(ValueError, self.data_access.get_users_report, "nkie", {}, bad_format_date)

        date_from = "2017-01-30 00:00:00.01"
        date_to = "2017-01-30 00:00:00"
        self.assertRaises(ValueError, self.data_access.get_users_report, "nkie", {}, date_from, date_to)

        date_to = datetime.now()
        date_from = datetime.now()
        self.assertRaises(ValueError, self.data_access.get_users_report, "nkie", {}, date_from, date_to)

        date_from = datetime.now()
        self.assertRaises(ValueError, self.data_access.get_users_report, "nkie", {}, date_from)

    def test_when_a_customer_or_an_event_is_not_in_the_data_base_then_a_key_error_is_raised(self):
        unknown_customers = ["pumba", "mcking", "dr.bob"]
        for unk_id in unknown_customers:
            self.assertRaises(KeyError, self.data_access.get_users_report, unk_id, {})

        unknown_events = {"step_3": [404, 10, 9901, 7]}
        self.assertRaises(KeyError, self.data_access.get_users_report, "nkie", unknown_events)
