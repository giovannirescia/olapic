# -*- coding: utf-8 -*-

"""Data access layer module."""
from collections import defaultdict
from dateutil.parser import parse

from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.sql import func

from conversion_funnel.models import Customer, Event, EventType


# creating and closing sessions
class DataBaseException(Exception):
    """Custom Exception for opening / closing a session.

    :param msg: the message for this exception
    :type msg: string
    :returns: an exception
    :rtype: DataBaseException
    """
    def __init__(self, *args, msg, **kwargs):
        Exception.__init__(self, *args, **kwargs)
        self.msg = msg


class DataAccess(object):
    """Data access layer for interact with the database.

    :public methods: get_users_report
    :Instance variables:
    :param db_engine: an engine
    :type db_engine: sqalchemy.engine.base.Engine
    """
    def __init__(self, db_engine):
        self._db_engine = db_engine
        # allow multiple session
        self._session_factory = sessionmaker(bind=db_engine, expire_on_commit=False)
        self._session = scoped_session(self._session_factory)

    def get_users_report(self, customer_id, event_types, date_from=None, date_to=None):
        """Given a customer and events, returns the number of users for each event.

        :param customer_id: the customer on interest
        :param event_types: a dict with the event type ids we are interested in
        :param date_from: the lower bound to filter the events by date, optional
        :param date_to: the upper bound to filter the events by date, optional
        :type customer_id: str
        :type event_types: dict of lists
        :type date_from: str
        :type date_to: str
        :returns: the number of users for each event
        :rtype: dict
        :Raises:
            TypeError: * customer_id type missmatch
                       * an event_id type missmatch
            ValueError: * ill formed date expression
                        * empty date range
            KeyError: * customer is not in the database
                      * an event type is not in the database
            DataBaseException: problems during creating or closing a session to access the database
        """
        if not isinstance(customer_id, str):
            raise TypeError("customer_id should be a string")
        for event_type in event_types.values():
            if not all(isinstance(event_type_id, int) for event_type_id in event_type):
                raise TypeError("events list should contain integers only")

        try:
            session = self._get_session()
        except:
            raise DataBaseException(msg="Error creating a session")

        for event_ids in event_types.values():
            for event_type_id in event_ids:
                if not session.query(EventType).filter_by(id=event_type_id).count():
                    raise KeyError("event {} is not in the database".format(event_type_id))

        if not session.query(Customer).filter_by(id=customer_id).count():
            raise KeyError("Customer {} not found".format(customer_id))

        if date_from is not None:
            try:
                date_from = parse(date_from)
            except:
                raise ValueError("Ill formed date_from expression, " +\
                "it could be formated like YYYY-MM-DD-HH:MM:SS.{x}")
        else:
            date_from = session.query(func.min(Event.timestamp)).one_or_none()[0]

        if date_to is not None:
            try:
                date_to = parse(date_to)
            except:
                raise ValueError("Ill formed date_to expression, " +\
                "it could be formated like YYYY-MM-DD-HH:MM:SS.{x}")
        else:
            date_to = session.query(func.max(Event.timestamp)).one_or_none()[0]

        if date_from >= date_to:
            raise ValueError("Empty date range, boundaries for customer {} are:\nMin: {}\nMax: {}".
                             format(customer_id, session.query(func.min(Event.timestamp)).
                                    filter(Event.customer_id == customer_id).one_or_none()[0],
                                    session.query(func.max(Event.timestamp)).
                                    filter(Event.customer_id == customer_id).one_or_none()[0]))

        step_1_event_ids = event_types.get("step_1", [])
        step_2_event_ids = event_types.get("step_2", [])
        step_3_event_ids = event_types.get("step_3", [])
        users_report = defaultdict(int)

        for event_type_id_1 in step_1_event_ids:
            q_1 = session.query(Event.user_id).\
                filter(Event.timestamp >= date_from, Event.timestamp <= date_to,
                       Event.customer_id == customer_id, Event.event_type_id == event_type_id_1)
            users_report['step_1'] += q_1.count()
            for event_type_id_2 in step_2_event_ids:
                q_2 = session.query(Event.user_id).filter(Event.timestamp >= date_from,
                                                          Event.timestamp <= date_to,
                                                          Event.customer_id == customer_id,
                                                          Event.event_type_id == event_type_id_2,
                                                          Event.user_id.in_(q_1))
                users_report['step_2'] += q_2.count()
                for event_type_id_3 in step_3_event_ids:
                    q_3 = session.query(Event.user_id).filter(Event.timestamp >= date_from,
                                                              Event.timestamp <= date_to,
                                                              Event.customer_id == customer_id,
                                                              Event.event_type_id == event_type_id_3,
                                                              Event.user_id.in_(q_2))
                    users_report['step_3'] += q_3.count()

        try:
            self._close_session(session)
        except:
            raise DataBaseException(msg="Error closing the session")
        finally:
            return users_report

    def add_object(self, obj):
        """Writes an objet to the database.

        :param obj: the object to add
        :type obj: any type accepted by the database being modeled
        :returns: None
        :Raises:
            * DataBaseException: error during the opening/closing of the session
        """
        session = self._get_session()
        session.add(obj)
        session.commit()
        self._close_session(session)

    def _get_session(self):
        """Opens an SQLAlchemy session.

        This function must be implemented because session doesn't support
        context manager.

        :returns: an SQLAlchemy session
        :rtype: sqlalchemy.orm.session.Session
        :Raises:
            * DataBaseException: error during the opening/closing of the session
        """
        try:
            session = self._session()
        except:
            raise DataBaseException(msg="Error creating a session")
        return session

    def _close_session(self, session):
        """Closes an SQLAlchemy session.

        This function must be implemented because session doesn't support
        context manager.

        :param session: an SQLAlchemy session
        :type session: sqlalchemy.orm.session.Session
        :returns: None
        :Raises:
            * DataBaseException: error during the opening/closing of the session
        """
        try:
            session.expunge_all()
            session.close()
        except:
            raise DataBaseException(msg="Error closing the session")
