# -*- coding: utf-8 -*-

"""The Models -Tables- for this database model."""
from sqlalchemy import (Column, Integer, String, DateTime, ForeignKey)
from sqlalchemy.orm import relationship, backref

from conversion_funnel.database import Base


class Customer(Base):
    """An Olapic customer."""
    __tablename__ = "customer"
    id = Column(u"id", String, primary_key=True)

    def __repr__(self):
        return "<Customer(customer_id={})>".format(self.id)


class User(Base):
    """A User."""
    __tablename__ = "user"
    id = Column(u"id", String, primary_key=True)

    def __repr__(self):
        return "<User(user_id={})>".format(self.id)


class EventType(Base):
    """A type of event id."""
    __tablename__ = "eventType"
    id = Column(u"id", Integer, primary_key=True)

    def __repr__(self):
        return "<EventType(event_type_id={})>".format(self.id)


class Device(Base):
    """The device that was used for some interaction, meant for compute some metrics."""
    __tablename__ = "device"
    id = Column(u"id", Integer, primary_key=True)

    def __repr__(self):
        return "<Device(device_id={})>".format(self.id)


class Event(Base):
    """The observed events.

    Each observed event counts wiht:
      * customer_id
      * timestamp
      * user_id
      * event_type_id
      * device_id
    """
    __tablename__ = "event"
    id = Column(u"id", Integer, primary_key=True)
    customer_id = Column(u"customer_id", String, ForeignKey("customer.id"))
    customer = relationship("Customer", lazy="subquery",
                            backref=backref("customer_id", cascade="all, delete"),
                            single_parent=True)

    timestamp = Column(u"timestamp", DateTime)

    user_id = Column(u"user_id", Integer, ForeignKey("user.id"))
    user = relationship("User", lazy="subquery",
                        backref=backref("user_id", cascade="all, delete"),
                        single_parent=True)

    event_type_id = Column(u"event_type_id", Integer, ForeignKey("eventType.id"))
    eventType = relationship("EventType", lazy="subquery",
                             backref=backref("event_type_id", cascade="all, delete"),
                             single_parent=True)

    device_id = Column(u"device_id", Integer, ForeignKey("device.id"))
    device = relationship("Device", lazy="subquery",
                          backref=backref("device_id", cascade="all, delete"), single_parent=True)

    def __repr__(self):
        return "<Event(customer_id={},\n\ttimestamp={},\n\tuser_id={},\n\tevent_type_id={},\
        \n\tdevice_id={})>".format(self.customer_id,
                                   self.timestamp,
                                   self.user_id,
                                   self.event_type_id,
                                   self.device_id,
                                  )
