# -*- coding: utf-8 -*-

"""Web service to get a users report."""
import logging
import argparse
from collections import OrderedDict

from flask import Flask, jsonify, request

from conversion_funnel.data_access import DataAccess
from conversion_funnel.data_access import DataBaseException
from conversion_funnel.database import db_connect


def get_data_access(engine=None):
    """Creates an instance of the data access layer.

    :param engine: an engine (optional)
    :type engine: sqalchemy.engine.base.Engine
    :returns: an instance of the DataAccess class
    :rtype: DataAccess
    """
    if engine is not None:
        data_access = DataAccess(engine)
    else:
        engine = db_connect()
        data_access = DataAccess(engine)

    return data_access


class InvalidUsage(Exception):
    """Custom exception for the API."""
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv["message"] = self.message
        return rv


def create_app(debug=True, engine=None):
    """Creates a Flask application.

    :param debug: whether run this app in debug mode, optional
    :param engine: an engine, optional
    :type debug: bool
    :type engine: sqalchemy.engine.base.Engine
    :returns: a Flask application
    :rtype: flask.app.Flask
    """
    app = Flask(__name__)
    app.debug = debug
    app.config["JSON_SORT_KEYS"] = False

    @app.errorhandler(InvalidUsage)
    def handle_invalid_usage(error):
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        return response

    @app.route("/shutdown", methods=["POST"])
    def shutdown():
        """Shutdown this server."""
        def shutdown_server():
            func = request.environ.get("werkzeug.server.shutdown")
            if func is None:
                raise RuntimeError("Not running with the Werkzeug Server")
            func()

        shutdown_server()
        return "Server shutting down..."

    @app.route("/olapic/api/v1.0/customers/<string:customer_id>/conversion_funnel",
               methods=["GET"])
    def get_users_report(customer_id):
        """Gets a users report for a given customer.

        :param customer_id: the customer of interest
        :type customer_id: str
        :returns: (JSON response, status code)
        :rtype: (JSON, int)
        """
        date_from = request.args.get("date_from", None)
        date_to = request.args.get("date_to", None)
        data_access = get_data_access(engine)
        event_dict = OrderedDict({"step_1": [11],
                                  "step_2": [13, 14, 15],
                                  "step_3": [1], })

        try:
            conv_fun = data_access.get_users_report(customer_id=customer_id,
                                                    event_types=event_dict,
                                                    date_from=date_from,
                                                    date_to=date_to)

        except TypeError as err:
            logging.error(msg=err)
            raise InvalidUsage(err.args[0], status_code=400)
        except KeyError as err:
            logging.error(msg=err)
            raise InvalidUsage(err.args[0], status_code=404)
        except ValueError as err:
            logging.error(msg=err)
            raise InvalidUsage(err.args[0], status_code=400)
        except DataBaseException as err:
            logging.error(msg=err.msg)
            raise InvalidUsage(err.args[0], status_code=500)

        data = OrderedDict({"data": OrderedDict({"customer": [customer_id],
                                                 "conversion_funnel": conv_fun, })})
        logging.info(msg="Returning JSON object...")

        return jsonify(data), 200

    return app


if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser(description="Runs a server with services for Olapic",
                                         formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    arg_parser.add_argument("-H", "--host", type=str,
                            default="0.0.0.0", help="The host to run this server on")
    arg_parser.add_argument("-P", "--port", type=int,
                            default=5000, help="The port to run this server on")
    arg_parser.add_argument("-D", "--debug", type=int,
                            default=1, help="Debug mode")

    args = vars(arg_parser.parse_args())
    debug = bool(args["debug"])
    app = create_app(debug=debug)
    host = args["host"]
    port = args["port"]
    app.run(host=host, port=port)
